---
title: Verification and Validation of Side-channel Freedom

description: |
  Securing systems against side-channel attacks.

people:
  - mehak
  - chundong

layout: project
last-updated: 2018-08-15
---

<p style="text-align:justify">
Micro-architectural timing channels are one of the best-known side channels exploited by attackers. The presence of such timing channels allows attackers to retrieve sensitive information by exploiting dynamic software properties (e.g. time, cache misses and memory access statistics). In the last two decades, the security research community has discovered numerous evidences of practical timing attacks, with more recent and critical attacks reflected in CacheBleed , Spectre and Meltdown. In this proposal, we will design a set of technologies and tools to validate and verify arbitrary programs against realistic timing-channel attacks. Our goal is not to design a set of techniques to exploit timing channels of a system. Instead, for arbitrary software programs, we will develop efficient algorithms to validate and verify them against realistic, timing-channel attack models. Driven by the results of our verification and validation, we will further synthesize countermeasures to shield program executions from timing-channel attacks. In such a fashion, for arbitrary programs, we will enable strong theoretical guarantees on the level of protection against threats involving timing channels.To know more about the topic, please take a look at our following publications: 
			<a href="https://arxiv.org/abs/1807.05843">arXiv-2018</a>, 
			<a href="https://sudiptac.bitbucket.io/papers/cachefix.pdf">TCAD/EMSOFT-2018</a>, 
			<a href="https://sudiptac.bitbucket.io/papers/chalice_extended.pdf">MEMOCODE-2017</a>, 
			<a href="https://sudiptac.bitbucket.io/papers/catapult-TR.pdf">TACAS-2017</a>, 
			<a href="https://sudiptac.bitbucket.io/papers/sbst_side_channel.pdf">ICSTW-2017</a>, 
			<a href="https://sudiptac.bitbucket.io/papers/sparta.pdf">ASPDAC-2016</a>.
</p>
