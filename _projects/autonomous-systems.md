---
title: Validation and Optimization of Autonomous Systems

description: |
  Validation and Verification of AI/ML-based Systems.

people:
  - chundong
  - sakshi
  - pryanshu
  - tanya

layout: project
last-updated: 2018-08-13
---
<p style="text-align:justify">

The aim of this project is to employ rigorous software 
engineering principles for designing robust decision 
making systems. To this end, we focus on various desirable 
properties of decision making systems, including but not 
limited to fairness (i.e. removing social discrimination) 
and robustness. To know more about the topic, take a look 
at the following publications: 

<a href="https://sudiptac.bitbucket.io/papers/aequitas.pdf">ASE-2018</a>, 
<a href="https://sudiptac.bitbucket.io/papers/raids.pdf">EMSOFT-WiP-2018</a>. 

</p>
